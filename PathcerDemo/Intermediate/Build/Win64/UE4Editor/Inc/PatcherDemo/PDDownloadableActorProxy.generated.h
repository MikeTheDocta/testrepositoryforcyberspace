// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PATCHERDEMO_PDDownloadableActorProxy_generated_h
#error "PDDownloadableActorProxy.generated.h already included, missing '#pragma once' in PDDownloadableActorProxy.h"
#endif
#define PATCHERDEMO_PDDownloadableActorProxy_generated_h

#define PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_SPARSE_DATA
#define PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_RPC_WRAPPERS
#define PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPDDownloadableActorProxy(); \
	friend struct Z_Construct_UClass_APDDownloadableActorProxy_Statics; \
public: \
	DECLARE_CLASS(APDDownloadableActorProxy, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PatcherDemo"), NO_API) \
	DECLARE_SERIALIZER(APDDownloadableActorProxy)


#define PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPDDownloadableActorProxy(); \
	friend struct Z_Construct_UClass_APDDownloadableActorProxy_Statics; \
public: \
	DECLARE_CLASS(APDDownloadableActorProxy, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PatcherDemo"), NO_API) \
	DECLARE_SERIALIZER(APDDownloadableActorProxy)


#define PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APDDownloadableActorProxy(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APDDownloadableActorProxy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APDDownloadableActorProxy); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APDDownloadableActorProxy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APDDownloadableActorProxy(APDDownloadableActorProxy&&); \
	NO_API APDDownloadableActorProxy(const APDDownloadableActorProxy&); \
public:


#define PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APDDownloadableActorProxy(APDDownloadableActorProxy&&); \
	NO_API APDDownloadableActorProxy(const APDDownloadableActorProxy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APDDownloadableActorProxy); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APDDownloadableActorProxy); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APDDownloadableActorProxy)


#define PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_PRIVATE_PROPERTY_OFFSET
#define PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_9_PROLOG
#define PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_PRIVATE_PROPERTY_OFFSET \
	PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_SPARSE_DATA \
	PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_RPC_WRAPPERS \
	PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_INCLASS \
	PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_PRIVATE_PROPERTY_OFFSET \
	PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_SPARSE_DATA \
	PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_INCLASS_NO_PURE_DECLS \
	PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PATCHERDEMO_API UClass* StaticClass<class APDDownloadableActorProxy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PathcerDemo_Source_PatcherDemo_PDDownloadableActorProxy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
