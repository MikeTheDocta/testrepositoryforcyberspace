// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PatcherDemo/PDDownloadableActorProxy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePDDownloadableActorProxy() {}
// Cross Module References
	PATCHERDEMO_API UClass* Z_Construct_UClass_APDDownloadableActorProxy_NoRegister();
	PATCHERDEMO_API UClass* Z_Construct_UClass_APDDownloadableActorProxy();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_PatcherDemo();
// End Cross Module References
	void APDDownloadableActorProxy::StaticRegisterNativesAPDDownloadableActorProxy()
	{
	}
	UClass* Z_Construct_UClass_APDDownloadableActorProxy_NoRegister()
	{
		return APDDownloadableActorProxy::StaticClass();
	}
	struct Z_Construct_UClass_APDDownloadableActorProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APDDownloadableActorProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_PatcherDemo,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APDDownloadableActorProxy_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PDDownloadableActorProxy.h" },
		{ "ModuleRelativePath", "PDDownloadableActorProxy.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APDDownloadableActorProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APDDownloadableActorProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APDDownloadableActorProxy_Statics::ClassParams = {
		&APDDownloadableActorProxy::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_APDDownloadableActorProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APDDownloadableActorProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APDDownloadableActorProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APDDownloadableActorProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APDDownloadableActorProxy, 277155000);
	template<> PATCHERDEMO_API UClass* StaticClass<APDDownloadableActorProxy>()
	{
		return APDDownloadableActorProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APDDownloadableActorProxy(Z_Construct_UClass_APDDownloadableActorProxy, &APDDownloadableActorProxy::StaticClass, TEXT("/Script/PatcherDemo"), TEXT("APDDownloadableActorProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APDDownloadableActorProxy);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
